<?php
    // $_GET and $_POST are 'super global' variables in PHP. It is always accessible regardless the scope.
    // "super global" variable allows data to persist in pages or a single session.

    // Both $_GET and $_POST create an array that holds key/value pairs, where:
        // "key" represents the name of the form control element
        // "value" represents the input data from the user
    
    // isset checks if the variable exists - true if variable is set, false if return is null.
    // this conditions avoid showing the warning at the start of the page

    $tasks = ['Get git', 'Bake HTML','Eat CSS','Learn PHP'];
    if(isset($_GET['index'])) {
        // echo $_GET['index'];
        // var_dump($_GET);
        $indexGet = $_GET['index'];
        echo "The retrieved task from GET is $tasks[$indexGet]
        <br>";
    }

    if(isset($_POST['index'])) {
        // echo $_POST['index'];
        // var_dump($_POST);
        $indexPost = $_POST['index'];
        echo "The retrieved task from POST is $tasks[$indexPost]
        <br>";
    }
   
    // warnings to show that no answer is selected therfore no index number to show
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S05: Client-Server Communication (GET and POST)</title>
    </head>
    <body>

        <h1>Task index from GET</h1>
        <form method="GET">

            Email: <input type="text" name="email"> <br>
            Password: <input type="password" name="password"> <br>

            <!-- GET automatically appends information on the URL (from select and option) - not sensitive -->
            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">GET</button>
            
        </form>

        <h1>Task index from POST</h1>
        <form method="POST">
            <!-- POST protects sensitive information -->
            <select name="index" required>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

            <button type="submit">POST</button>
            
        </form>

        <!-- no PUT and DELETE they are considered as sub-methods of POST;  -->
        
    </body>
</html>
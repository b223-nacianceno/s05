<?php

session_start();

// echo $_POST['description'];
// echo $_SESSION['greet'];

class TaskList {
    // Add Task
    public function add($description){
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false,
        ];

        // If there is no added task yet.
        if ($_SESSION['tasks'] === null){
            $_SESSION['tasks'] = array();
            // $_SESSION['tasks'] = [];
        }

        // Then $newTask will be added in the $_SESSION['tasks'] variable.

        array_push($_SESSION['tasks'], $newTask);
    }

    // Update a Task
    // The updated task will be needing three parameters:
        // $id for searching specific task
        // $description and $isFinished

    public function update($id, $description, $isFinished){
        $_SESSION['tasks'][$id]->description = $description;

        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null);
    }

    // Delete a task

    public function remove($id){
        // array_splice(array, startDelete, length, newArrElement(optional))
        array_splice($_SESSION['tasks'], $id, 1);
    }

    // Remove all the tasks
    public function clear($id){
        // deletes all data associated with the current session
        session_destroy();
    }
}

// taskList is instantiated from the TaskList() class to have access to its method.
$taskList = new TaskList();

// This will handle the action sent by the user.
if($_POST['action'] === 'add') {
    $taskList->add($_POST['description']);
}

else if ($_POST['action'] === 'update') {
    $taskList->update($_POST['id'],$_POST['description'], $_POST['isFinished']);
}

else if ($_POST['action'] === 'remove') {
    $taskList->remove($_POST['id']);
}

else if ($_POST['action'] === 'clear') {
    $taskList->clear();
}


// it will redirect us to the index file upon sending the request
header('Location: ./index.php');